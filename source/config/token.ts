import { Request, Response, NextFunction } from 'express'
import 'dotenv/config'
import connection from './database'
import { SessionStore } from '../models/Session'
const checkToken = async (req: Request, res: Response, next: NextFunction) => {
  const apikey: any = req.headers.x_api_key
  const token: any = req.headers.token

  if (apikey) {
    try {
      const apikey = process.env.APLICATIONTOKEN
      if (apikey == apikey) {
        if (token) {
          try {
            const checksessionlog = await connection.manager
              .findOneBy(SessionStore, { token: token })
              .then((data) => {
                return data?.token
              })
              .catch((error) => {
                return error
              })
            if (token != checksessionlog) {
              return res.status(500).send({ message: 'token has blocked!' })
            } else {
              next()
            }
          } catch (error) {
            return res.status(500).send({
              message: 'Internal Server Error',
            })
          }
          // check session login
        } else {
          return res.json({
            error: 403,
            message: 'Access Denied! Unauthorized User',
          })
        }
      } else {
        return res.status(403).send({
          message: 'Your API Key is Invalid!',
        })
      }
    } catch (error) {
      return res.status(500).send({
        message: 'Internal Server Error',
      })
    }
    // check session login
  } else {
    return res.json({
      error: 403,
      message: 'Please input your API KEY',
    })
  }
}
export default checkToken
