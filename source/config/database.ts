import 'reflect-metadata'
import { DataSource } from 'typeorm'
import 'dotenv/config'
import { Users } from '../models/User'
import { UserRole } from '../models/UserRole'
import { SessionStore } from '../models/Session'
import { Banner } from '../models/Banner'

const connection = new DataSource({
  type: 'mysql',
  host: `${process.env.DBHOST}`,
  username: `${process.env.DBUSER}`,
  password: `${process.env.DBPASS}`,
  database: `${process.env.DBDATABASES}`,
  synchronize: true,
  logging: false,
  entities: [
    Users,
    UserRole,
    SessionStore,
    Banner,
  ],
  migrations: [],
  subscribers: [],
  extra: {
    insecureAuth: true,
  },
})
connection.initialize().then(() => {
  // here you can start to work with your database
})

export default connection
