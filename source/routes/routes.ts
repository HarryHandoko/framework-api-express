import express from 'express'
import checkToken from '../config/token'
import checkKey from '../config/api-key'
import { AdminController } from '../controllers/Admin/AdminController'
import { BannerController } from '../controllers/Admin/BannerController'
import { UserController } from '../controllers/Admin/UserController'
import { SettingController } from '../controllers/Admin/SettingController'
import { BlogsController } from '../controllers/Admin/BlogController'
import { RoleController } from '../controllers/Auth/RoleController'
import { AuthController } from '../controllers/Auth/AuthController'
const HomeController = require('../controllers/HomeController')

const appsRoute = express()

/**
 *
 * !this for user route
 * route resource for admin management
 * todo: always use route midleware: 'checkKey'
 * * Important 'routerHome' for declare route
 * only one methode 'GET' can be used in this route
 *
 * **/
/**
 * * @types default routerHome
 */

const routerHome = express.Router()

//Home
routerHome.route('/home').get(checkKey, HomeController.index)

//Settings
routerHome.route('/setting').get(checkKey, SettingController.index)

//Banner
routerHome.route('/banner').get(checkKey, BannerController.index)

//Blogs
routerHome.route('/blogs').get(checkKey, BlogsController.index)
//togetDetail
routerHome.route('/blogs/:slugs').get(checkKey, BlogsController.detail)

/**
 *
 * !this for admni route
 * route resource for admin management
 * todo: always use route midleware: checkToken
 * * Important 'routeAdmin' for declare route
 * methode GET,POST,PUT,PATCH,DELETE,HEAD,OPTIONS can be used
 *
 * **/
/**
 * * @types default routerAdmin
 */

const routerAdmin = express.Router()

// login to get authentication token
routerAdmin.route('/login').post(checkKey, AuthController.login)
routerAdmin.route('/register').post(checkKey, AuthController.register)
routerAdmin.route('/logout').delete(checkToken, AuthController.logout)

routerAdmin.route('/role').post(checkKey, RoleController.create)

routerAdmin.route('/blogs').get(checkToken, BlogsController.index)
//togetDetail
routerAdmin.route('/blogs/:slugs').get(checkToken, BlogsController.detail)

/**
 * ?for landing page admin
 **/
routerAdmin
  .route('/webmin')
  .get(checkToken, AdminController.index)
  .post(checkToken, AdminController.createUser)

/**
 *
 *  !For user managements like USER ROLE for user
 *
 **/
routerAdmin
  .route('/webmin/user-managet')
  .get(checkToken, UserController.getuser)

/**
 *  todo admin banner manager
 **/
routerAdmin
  .route('/banner')
  .get(checkToken, BannerController.index)
  .post(checkToken, BannerController.create)

routerAdmin
  .route('/banner/:id')
  .get(checkToken, BannerController.getById)
  .put(checkToken, BannerController.update)

/**
 * todo blog management for admin
 */
routerAdmin
  .route('/blogs')
  .get(checkToken, BlogsController.index)
  .post(checkToken, BlogsController.create)
routerAdmin
  .route('/blogs/:slugs')
  .get(checkToken, BlogsController.detail) //for get deail blog
  .put(checkToken) //for update blogs

/**
 * todo end blogs
 */

/**
 * ! settings web profile
 */
routerAdmin
  .route('setting')
  .get(checkToken, SettingController.index)
  .post(checkToken, SettingController.create)

// todo for admin prefix
appsRoute.use('/admin', routerAdmin)
// todo for user prefix
appsRoute.use('/users', routerHome)

export default appsRoute
