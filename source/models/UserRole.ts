/**
 * for role management users
 */

import { MinLength } from 'class-validator'
import moment from 'moment'
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  BeforeInsert,
  BeforeUpdate,
} from 'typeorm'
import { Users } from './User'

@Entity()
export class UserRole {
  @PrimaryGeneratedColumn()
  id!: number

  @Column()
  @MinLength(5)
  role!: string

  @Column()
  @MinLength(4)
  Description!: string

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  public Created_at!: Date

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public Updated_at!: Date

  @OneToMany(() => Users, (user) => user.Role)
  user!: Users['Role']
}
