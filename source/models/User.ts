/**
 * for user management
 */

import moment from 'moment'
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  BeforeInsert,
  BeforeUpdate,
  ManyToMany,
} from 'typeorm'
import {
  Contains,
  IsInt,
  Length,
  IsEmail,
  IsFQDN,
  IsDate,
  Min,
  Max,
  IsString,
  min,
  MinLength,
  MaxLength,
  IsNotEmpty,
} from 'class-validator'

import { Blogs } from './Blogs'
import { UserRole } from './UserRole'

@Entity()
export class Users {
  @PrimaryGeneratedColumn()
  id!: number

  @Column()
  @IsNotEmpty()
  @MinLength(3)
  Name!: string

  @Column()
  @IsNotEmpty()
  @MinLength(6)
  Username!: string

  @Column('varchar')
  @IsNotEmpty()
  @IsEmail()
  Email!: string

  @Column()
  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  @MaxLength(64)
  Password!: string

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  public Created_at!: Date

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public Updated_at!: Date

  @ManyToOne(() => UserRole, (role) => role.id)
  Role!: UserRole['id']
}
