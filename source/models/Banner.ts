/**
 * for banner models
 */

import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  BeforeInsert,
  BeforeUpdate,
} from 'typeorm'
import { UserRole } from './UserRole'
import moment from 'moment'
import { IsNotEmpty } from 'class-validator'

@Entity()
export class Banner {
  @PrimaryGeneratedColumn()
  id!: number

  @Column()
  @IsNotEmpty()
  Name!: string

  @Column()
  @IsNotEmpty()
  Image!: string

  @Column()
  Description!: string

  @Column({ default: 'show' })
  Status!: string

  @Column('varchar')
  @IsNotEmpty()
  Type!: string

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  public Created_at!: Date

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public Updated_at!: Date
}
