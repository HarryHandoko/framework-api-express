/**
 * for authentication user session
 */

import { IsNotEmpty } from 'class-validator'
import moment from 'moment'
import { TextDataType } from 'sequelize'
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  Long,
  CreateDateColumn,
  UpdateDateColumn,
  BeforeInsert,
  BeforeUpdate,
} from 'typeorm'

@Entity()
export class SessionStore {
  @PrimaryGeneratedColumn()
  id!: number

  @Column('longtext')
  @IsNotEmpty()
  token!: string | undefined

  @Column()
  @IsNotEmpty()
  session!: string

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  public Created_at!: Date

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  public Updated_at!: Date
}
