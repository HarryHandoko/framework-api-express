import { Request, Response } from 'express'
import * as fs from 'fs'

import multer, { FileFilterCallback } from 'multer'
import { basename } from 'path'
const path = require('path')

type DestinationCallback = (error: Error | null, destination: string) => void
type FileNameCallback = (error: Error | null, filename: string) => void

export class FileStore {
  static storage = multer.diskStorage({
    destination: (
      request: Request,
      file: Express.Multer.File,
      callback: DestinationCallback,
    ): void => {
      let req_url: any = request.path.split('/')
      let pathname = req_url[2]
      let newPath = path.join(__dirname + `./../public/${pathname}/`)
      if (!fs.existsSync(newPath)) {
        fs.mkdirSync(newPath, { recursive: false })
      }
      return callback(null, newPath)
    },
    filename: (
      req: Request,
      file: Express.Multer.File,
      callback: FileNameCallback,
    ): void => {
      return callback(null, Date.now() + file.originalname)
    },
  })
  static fileFilter = (
    request: Request,
    file: Express.Multer.File,
    callback: FileFilterCallback,
  ): void => {
    if (
      file.mimetype === 'image/png' ||
      file.mimetype === 'image/jpg' ||
      file.mimetype === 'image/jpeg'
    ) {
      callback(null, true)
    } else {
      callback(null, false)
    }
  }
  static maxSize: any = 8 * 1024 * 10

  static upload = multer({
    storage: this.storage,
    limits: this.maxSize,
    fileFilter: this.fileFilter,
  }).fields([{ name: 'image' }])
}
