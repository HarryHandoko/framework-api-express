function getOffset(currentPage: number = 1, listPerPage: any) {
  return (currentPage - 1) * listPerPage.length;
}

function emptyOrRows(rows: any) {
  if (!rows) {
    return [];
  } else {
    return rows;
  }
}

module.exports = {
  getOffset,
  emptyOrRows,
};
