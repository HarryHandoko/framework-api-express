import { Request, Response } from "express";
import connection from "../../config/database";
import { Banner } from "../../models/Banner";
const helper = require("../../service/helper");
import { validate } from "class-validator";

const directory = "public/image/";

export class BannerController {
  static index = async (req: Request, res: Response) => {
    try {
      const data = await connection.query("SELECT * FROM banner");
      return res.status(200).json({ data: data, message: "Success get data", statusCode: 200 });
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  static getById = async (req: Request, res: Response) => {
    try {
      const data = await connection.query('SELECT * FROM banner where id = "' + req.params.id + '"');
      return res.status(200).json({
        data: data,
        message: "Success get data",
        statusCode: 200
      });
    } catch (error) {
      return res.status(500).send(error);
    }
  };

  static create = async (req: Request, res: Response) => {
    try {
      let { name, description, type } = req.body;
      let { image }: any = req.files;
      console.log(image);
      const bannerReq = new Banner();
      bannerReq.Name = name;
      bannerReq.Description = description;
      bannerReq.Image = image[0].filename;
      bannerReq.Type = type;
      const isValid = await validate(bannerReq);
      if (isValid.length > 0) {
        throw new Error(`Validation failed! ` + isValid);
      } else {
        const result = await connection.manager
          .save(bannerReq)
          .then((results) => {
            return results;
          })
          .catch((error) => {
            return console.log(error);
          });
        const data = helper.emptyOrRows(result);
        if (data.length != 0) {
          return res.json({
            status: 201,
            message: "Created data successfully banner",
            data: result
          });
        }
      }
    } catch (err) {
      return res.status(500).json({
        error: `${err}`
      });
    }
  };

  static update = async (req: Request, res: Response) => {
    try {
      let { name, description, type } = req.body;
      let { image }: any = req.files;

      if (image != null) {
        const bannerReq = new Banner();
        bannerReq.Name = name;
        bannerReq.Description = description;
        bannerReq.Image = image[0].filename;
        bannerReq.Type = type;
        const isValid = await validate(bannerReq);
        if (isValid.length > 0) {
          throw new Error(`Validation failed! ` + isValid);
        } else {
          const result = await connection
            .createQueryBuilder()
            .update(Banner)
            .set(bannerReq)
            .where("id = :id", { id: req.params.id })
            .execute()
            .then((results) => {
              return results;
            })
            .catch((error) => {
              return console.log(error);
            });
          const data = helper.emptyOrRows(result);
          if (data.length != 0) {
            return res.json({
              status: 202,
              message: "Updated data successfully ",
              data: result
            });
          }
        }
      } else {
        const data = await connection.query('SELECT * FROM banner where id = "' + req.params.id + '"');
        const bannerReq = new Banner();
        bannerReq.Name = name;
        bannerReq.Description = description;
        bannerReq.Image = data[0].Image;
        bannerReq.Type = type;
        const isValid = await validate(bannerReq);
        if (isValid.length > 0) {
          throw new Error(`Validation failed! ` + isValid);
        } else {
          const result = await connection
            .createQueryBuilder()
            .update(Banner)
            .set(bannerReq)
            .where("id = :id", { id: req.params.id })
            .execute()
            .then((results) => {
              return results;
            })
            .catch((error) => {
              return console.log(error);
            });
          const data = helper.emptyOrRows(result);
          if (data.length != 0) {
            return res.json({
              status: 202,
              message: "Updated data successfully ",
              data: result
            });
          }
        }
      }
    } catch (err) {
      return res.status(500).json({
        error: `${err}`
      });
    }
  };
}
