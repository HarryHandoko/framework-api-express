import { Request, Response } from "express";
import connection from "../../config/database";
import { Users } from "../../models/User";

export class UserController {
  static getResponse = (req: Request, res: Response) => {
    return res.json({ mes: "oke" });
  };

  static getuser = async (req: Request, res: Response) => {
    try {
      const database = await connection;
      const getusers =  await database.manager.find(Users);
      return res.json({ user: getusers });
    } catch (error) {
      res.send({ message: error });
    }
  };
}
