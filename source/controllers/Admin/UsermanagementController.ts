import { Request, Response } from "express";
import connection from "../../config/database";
import { UserRole } from "../../models/UserRole";

export class UserController {
  static getResponse = (req: Request, res: Response) => {
    return res.json({ mes: "oke" });
  };

  static getuser = async (req: Request, res: Response) => {
    try {
      const database = connection;
      const getusers =  await database.manager.find(UserRole);
      return res.json({ user: getusers });
    } catch (error) {
      res.send({ message: error });
    }
  };

  static createRole = async (req: Request, res: Response) => {

    
    try {
      const database = connection;
      const getusers =  await database.manager.find(UserRole);
      return res.json({ user: getusers });
    } catch (error) {
      res.send({ message: error });
    }
  };
}
