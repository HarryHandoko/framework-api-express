import { validate } from 'class-validator'
import { Request, Response } from 'express'
import { stringify } from 'querystring'

const { hashSync, genSaltSync, compare, compareSync } = require('bcrypt')
const { sign } = require('jsonwebtoken')
require('dotenv').config()
const APP_KEY = process.env.APLICATIONTOKEN
const helper = require('../../service/helper')
import DB from '../../config/database'
import { SessionStore } from '../../models/Session'
import { Users } from '../../models/User'
import { UserRole } from '../../models/UserRole'

export class AuthController {
  // login user or admin
  static login = async (req: Request, res: Response) => {
    try {
      const body = req.body

      // parsing form body
      const { username, password } = body
      const connection = DB
      // check session login
      const checksessionlog = await connection.manager.findOneBy(SessionStore, {
        session: username,
      })
      if (checksessionlog != null) {
        return res.status(200).send({
          code: 200,
          message: 'you has been loggin',
        })
      } else {
        // check user is registered or not
        const results = await connection.manager.findOneBy(Users, {
          Username: username,
        })
        const data = helper.emptyOrRows(results)

        if (data.length != 0) {
          const checkPass = await compare(password, results?.Password)
            .then((result: any) => {
              return result
            })
            .catch((error: any) => {
              return console.log(error)
            })

          if (checkPass) {
            const jsontoken = sign({ result: data }, results?.Username, {
              expiresIn: '1h',
            })
            const sess = new SessionStore()
            sess.token = jsontoken
            sess.session = results?.Username!
            let respSession = await connection.manager
              .save(sess)
              .then((session) => {
                return session
              })
              .catch((error) => {
                return error
              })
            return res.status(200).json({
              code: 200,
              message: 'OK',
              data: respSession,
            })
          } else {
            return res.send({
              code: 400,
              message: 'BAD_REQUEST',
              errors: { credentials: ['Wrong Password!'] },
            })
          }
        } else {
          return res.send({
            code: 400,
            message: 'BAD_REQUEST',
            errors: { credentials: ['Username Not Registered!'] },
          })
        }
      }
    } catch (error) {
      return res.status(500).json({
        error: error,
      })
    }
  }

  // registration user
  static register = async (req: Request, res: Response) => {
    try {
      const body = req.body
      const salt = genSaltSync(10)

      body.password = hashSync(body.password, salt)

      const connection = DB
      const { namalengkap, username, password, email } = body
      const results = await connection.manager.findOneBy(Users, {
        Username: username,
      })

      const data = helper.emptyOrRows(results)

      console.log(data.length)

      if (data.length == 0) {
        const roleid = await connection.query(
          'SELECT * FROM  user_role where role = "User"',
        )
        let users = new Users()
        users.Name = namalengkap
        users.Username = username
        users.Password = password
        users.Email = email
        users.Role = 2
        const isValid = await validate(users)
        console.log(isValid)
        if (isValid.length > 0) {
          throw new Error(`Validation failed!`)
        } else {
          const result = await connection.manager
            .save(users)
            .then((results) => {
              return results
            })
            .catch((error) => {
              return console.log(error)
            })

          const data = helper.emptyOrRows(result)
          if (data.length != 0) {
            return res.json({
              status: 200,
              message: 'register successfully',
            })
          }
        }
      } else {
        return res.json({ message: 'username has registered', status: 400 })
      }
    } catch (error) {
      return res.status(500).json({
        data: error,
        error: error,
      })
    }
  }

  static logout = async (req: Request, res: Response) => {
    try {
      const body = req.body

      // parsing form body
      const { username } = body
      const connection = DB
      // check session login
      const checksessionlog = await connection.manager.findOneBy(SessionStore, {
        session: username,
      })
      if (checksessionlog != null) {
        const sessionDelete = await connection.manager.delete(
          SessionStore,
          checksessionlog.id,
        )
        if (sessionDelete)
          return res.status(200).send({
            code: 200,
            message: 'you has been logout!!',
          })
        // check user is registered or not
      } else {
        return res.send({
          code: 400,
          message: 'BAD_REQUEST',
          errors: { credentials: ['You have not log in!'] },
        })
      }
    } catch (error) {
      return res.status(500).json({
        error: error,
      })
    }
  }
}
