import { validate } from 'class-validator'
import { Request, Response } from 'express'
import { stringify } from 'querystring'
import connection from '../../config/database'

const { hashSync, genSaltSync, compare, compareSync } = require('bcrypt')
const { sign } = require('jsonwebtoken')
require('dotenv').config()
const APP_KEY = process.env.APLICATIONTOKEN
const helper = require('../../service/helper')
import DB from '../../config/database'
import { SessionStore } from '../../models/Session'
import { UserRole } from '../../models/UserRole'

export class RoleController {
    static create = async (req: Request, res: Response) => {
        const { role, description } = req.body
        try {
          const RoleReq = new UserRole()
          RoleReq.role = role
          RoleReq.Description = description
          const result = await connection.manager
            .save(RoleReq)
            .then((results) => {
              return results
            })
            .catch((error) => {
              return console.log(error)
            })
          const data = helper.emptyOrRows(result)
          if (data.length != 0) {
            return res.json({
              status: 201,
              message: 'Created data successfully',
              data: result,
            })
          }
        } catch (error) {
          return res.status(500).send(error)
        }
      }

}
