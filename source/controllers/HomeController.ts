import { Request, Response } from "express";
const helper = require("../service/helper");
import DB from "../config/database";
import { Users } from "../models/User";

exports.index = async (req: Request, res: Response) => {
  // try {
  const connection = await DB;
  const [results, field] = await connection.manager.find(Users);
  const data = await helper.emptyOrRows(results);
  return res.status(200).json(data);
  // } catch (error) {
  //   return res.status(500).json(error);
  // }
};
