import router from './source/routes/routes'
import fastifyExpress from 'fastify-express'
import Fastify from 'fastify'
import xss from 'x-xss-protection'
import BodyParser from 'body-parser'
import Cors from 'cors'
import { FileStore } from './source/service/__files'
require('dotenv').config()

import 'reflect-metadata'
import connection from './source/config/database'
import multer from 'multer'
import path from 'path'

// database
// const connection = require("./source/config/database");

const cors = Cors()
const bodyparser = BodyParser.json()
const urlencodedParser = BodyParser.urlencoded({ extended: true })
const fastify = Fastify({ logger: true })
const xss_protection = xss()
import * as fs from 'fs'

let newPath = path.join(__dirname + '/source/public')
if (!fs.existsSync(newPath)) {
  fs.mkdirSync(newPath, { recursive: false })
}

fastify.register(fastifyExpress).after(() => {
  fastify.use(xss_protection)
  fastify.use(bodyparser)
  fastify.use(cors)
  fastify.use(urlencodedParser)
  fastify.register(require('fastify-typeorm-plugin'), {
    connection,
  })

  fastify.register(require('@fastify/static'), {
    root: path.join(__dirname, 'source/public/'),
    prefix: '/assets',
    decorateReply: true,
  })

  fastify.use(FileStore.upload)

  fastify.use(router)
  fastify.express.disabled('x-powered-by') // true
})
// fastify.ready()

fastify.listen(3001, async function (err, address) {
  address = '0.0.0.0'
  if (err) {
    fastify.log.error(err)
    process.exit(1)
  }
  // await connection;

  fastify.log.info(`server listening on ${address}`)
})
// })
// .catch((err: any) => console.log("Error: " + err));

// })
